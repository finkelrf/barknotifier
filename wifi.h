#ifndef WIFI_H
#define WIFI_H
#include <Arduino.h>
#include <ESP8266WiFi.h>

void setup_wifi(const char *wifi_ssid, const char *wifi_pass);

#endif