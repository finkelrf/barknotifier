#include "wifi.h"

#define WIFI_SSID "hotspot do fink"
#define WIFI_PASS "123456789"

// the setup function runs once when you press reset or power the board
void setup()
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  Serial.println("\nVersion: 2");
  setup_wifi(WIFI_SSID, WIFI_PASS);
}

// the loop function runs over and over again forever
void loop()
{
  digitalWrite(2, HIGH); // turn the LED on (HIGH is the voltage level)
  delay(1000);           // wait for a second
  digitalWrite(2, LOW);  // turn the LED off by making the voltage LOW
  delay(1000);           // wait for a second
}
